from sys import argv
from getopt import getopt
import wget

argList = argv[1:]

options = "hu:"
whole_options = ["help", "url"]

try :
    arg, val = getopt(argList, options, whole_options)

    for curr_arg, curr_val in arg :
        if curr_arg in ("-h", "--help") :
            print('''
            PYTHON CMD UTILITY - DOWNLOAD IMAGES
            Options:
            -h --help   Display this menu
            -u --url    set source url link
            **NOTE: Utility made only for testing purposes.
            Please avoid using in production**
            ''')

        elif curr_arg in ("-u", "--url=") :
            url_link = curr_val
            filename = wget.download(url_link, "./wget_download")
            print(filename)
    

except getopt.err as err :
    print(str(err))
